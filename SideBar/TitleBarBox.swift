//
//  TitleBarBox.swift
//  SideBar
//
//  Created by Franz Murner on 27.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa


class TitleBarBox: NSView {
    
    private var boxLayer = CALayer()
    private var titleBar = NSView()
    private var titleBarLabel = NSTextField(labelWithString: "")
    private var effectView = NSVisualEffectView()
    
    
    var fillColor: NSColor = .white{
        didSet{
            boxLayer.backgroundColor = fillColor.cgColor
        }
    }
    
    var titleBarColor: NSColor = .red{
        didSet{
            setTitlebarColor(color: titleBarColor)
        }
    }
    
    var titleBarTextColor: NSColor = .white{
        didSet{
            titleBarLabel.textColor = titleBarTextColor
        }
    }
    
    var titleBarIsHidden: Bool = false{
        didSet{
            titleBar.isHidden = titleBarIsHidden
        }
    }
    
    var titleBarText: String = "Placeholder"{
        didSet{
            setupTitleBarLabel(text: titleBarText)
        }
    }
    
    var titleBarHeight: CGFloat = 30
    
    var hasBlurEffect: Bool = false{
        didSet{
            effectView.isHidden = !hasBlurEffect
        }
    }
    
    override var intrinsicContentSize: NSSize{
        var size = super.intrinsicContentSize
        size.height += 500
        size.width = 20
        
        return size
    }

    
    
    init(color: NSColor){
        self.fillColor = color
        super.init(frame: .zero)
        boxLayer.backgroundColor = fillColor.cgColor
        boxLayer.cornerRadius = 20
        boxLayer.masksToBounds = true
        layer = boxLayer
        
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        addTitleBar()
        addTitlebarLable()
        

        
        
        
        
        

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    private func addTitleBar(){
        
        

        titleBar.translatesAutoresizingMaskIntoConstraints = false



        setTitlebarColor(color: titleBarColor)
        
        self.addSubview(titleBar)
        setupTitleBarLabel(text: titleBarText)
        titleBar.addSubview(titleBarLabel)
        
    
        
        let center = NSLayoutConstraint(item: titleBar,
                                        attribute: .centerX,
                                        relatedBy: .equal,
                                            toItem: self,
                                            attribute: .centerX,
                                            multiplier: 1,
                                            constant: 0)
        
        let top = NSLayoutConstraint(item: titleBar,
                                     attribute: .top,
                                     relatedBy: .equal,
                                            toItem: self,
                                            attribute: .top,
                                            multiplier: 1,
                                            constant: 0)
        
        let width = NSLayoutConstraint(item: titleBar,
                                     attribute: .width,
                                     relatedBy: .equal,
                                            toItem: self,
                                            attribute: .width,
                                            multiplier: 1,
                                            constant: 0)
        
        let height = NSLayoutConstraint(item: titleBar,
                                     attribute: .height,
                                     relatedBy: .equal,
                                            toItem: nil,
                                            attribute: .notAnAttribute,
                                            multiplier: 1,
                                            constant: 30)
        self.addConstraints([center, top, width, height])
        
        
        
    }
    
    func addTitlebarLable(){
        titleBar.addSubview(titleBarLabel)
        titleBarLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let center = NSLayoutConstraint(item: titleBarLabel,
                                        attribute: .centerY,
                                        relatedBy: .equal,
                                            toItem: titleBar,
                                            attribute: .centerY,
                                            multiplier: 1,
                                            constant: 0)
        
        let left = NSLayoutConstraint(item: titleBarLabel,
                                        attribute: .left,
                                        relatedBy: .equal,
                                            toItem: titleBar,
                                            attribute: .left,
                                            multiplier: 1,
                                            constant: 20)
        
        
        titleBar.addConstraints([center, left])

        
        setupTitleBarLabel(text: titleBarText)
        
        
    }
    
    
    
    
    private func setTitlebarColor(color: NSColor){
        let barLayer = CALayer()
        barLayer.backgroundColor = color.cgColor
        titleBar.layer = barLayer
    }
    
    func setupTitleBarLabel(text: String){
                
       let paragaphStyle = NSMutableParagraphStyle()
       paragaphStyle.alignment = .justified
        
        let font = NSFont.systemFont(ofSize: 20, weight: .bold)

        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .paragraphStyle: paragaphStyle,
        ]
        
        
        let attributetString = NSAttributedString(string: text, attributes: attributes)
        
        titleBarLabel.attributedStringValue = attributetString
        titleBarLabel.textColor = titleBarTextColor

    }
    


    
    
    
}
