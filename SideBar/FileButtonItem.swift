//
//  Item.swift
//  SideBar
//
//  Created by Franz Murner on 19.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

class FileButtonItem: NSCollectionViewItem {
    
    var button: FileButton!
    var label: NSTextField?
    var specificURL: URL?{
        didSet{
            button.connectedURL = specificURL
            addLabel()


        }
    }
    

    override func loadView() {
        
        let view = NSView(frame: NSRect.zero)
        
        button = FileButton(frame: NSRect(origin: CGPoint(x: 0  , y: 35), size: CGSize(width: 70, height: 70)))
        button.title = ""
        button.isBordered = false
        button.imageScaling = .scaleAxesIndependently
        button.image = NSImage(named: NSImage.actionTemplateName)
        button.bezelStyle = .shadowlessSquare
        
        view.addSubview(button)
        
        self.view = view
    }
    
    private func addLabel(){
        label?.removeFromSuperview()
        label = NSTextField(labelWithAttributedString: attributedStringForField(string: specificURL?.lastPathComponent ?? ""))
        
        guard let label = label else { return }
        
        label.setFrameOrigin(NSPoint(x: 35 - label.frame.size.width / 2, y: 20 - label.frame.size.height / 2))
        label.cell?.backgroundStyle = .emphasized
        label.textColor = .white
        
        view.addSubview(label)
    }
    
    func attributedStringForField(string: String)->NSAttributedString{
        
        
        let prefix = String(string.prefix(8))
        var lineBreak = "\n"
        var suffix = ""
        
        if string.count > 8{
            suffix = String(string.suffix(string.count - 8))
            lineBreak = "\n"
            
        }
        
        let paragaphStyle = NSMutableParagraphStyle()
        paragaphStyle.alignment = .center
        
        
        let attributes: [NSAttributedString.Key: Any] = [
            .paragraphStyle: paragaphStyle,
        ]
        
        return NSAttributedString(string: "\(prefix)\(lineBreak)\(suffix)", attributes: attributes)
    }

    
}
