//
//  Item.swift
//  SideBar
//
//  Created by Franz Murner on 19.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

class FileButtonItem: NSCollectionViewItem {
    
    var buttonII: FileButton!
    var specificURL: URL?{
        didSet{
            buttonII.connectedURL = specificURL
        }
    }
    
    override func viewDidLoad() {
    }


    override func loadView() {
        
        let view = NSView(frame: NSRect.zero)
        
        buttonII = FileButton(frame: NSRect(origin: CGPoint(x: 0  , y: 0), size: CGSize(width: 100, height: 20)))
        
        view.addSubview(buttonII)
        
        self.view = view
    }

    
}
