//
//  Container Controller.swift
//  SideBar
//
//  Created by Franz Murner on 19.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa


class ContainerController: NSView {
    
    private var contentView: NSView!
    private var scrollView: NSScrollView!
//    private var containers = [FileContainer]()
    private var activatedContainerPaths = [URL]()
    private var editButton: RoundTextButton!
    private var addButton: RoundTextButton!
    
    var tbb: TitleBarBox!
    
    private var containerPosY: CGFloat = 0
    


    

    
    
    

    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        let lay = CALayer()
        lay.backgroundColor = NSColor.gray.withAlphaComponent(0.01).cgColor
        self.layer = lay
        addContentScrollView()
        
        
        tbb = TitleBarBox(color: .black)
        contentView.addSubview(tbb)
        
        let top = NSLayoutConstraint(item: tbb,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: contentView,
                                     attribute: .top,
                                     multiplier: 1,
                                     constant: 0)
        
        let width = NSLayoutConstraint(item: tbb,
                                       attribute: .width,
                                       relatedBy: .equal,
                                            toItem: contentView,
                                            attribute: .width,
                                            multiplier: 1,
                                            constant: 0)
        
        contentView.addConstraints([top, width])
        
        
    
                                            
                                            
                                            
        
        
        

        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addContentScrollView(){
        
        contentView = NSView(frame: NSRect(origin: .zero, size: NSSize(width: self.frame.size.width, height: 2000)))
        scrollView = NSScrollView(frame: CGRect(origin: .zero, size: frame.size))
        scrollView.documentView = contentView
        scrollView.drawsBackground = false
        scrollView.hasVerticalScroller = false
        scrollView.documentView?.scroll(CGPoint(x: 0, y: contentView.frame.size.height))
        

        self.addSubview(scrollView)
        
    }

    public func addURL(url: URL){
        activatedContainerPaths.append(url)
    }
    
    public func showContainers(){
        
        let containers = ContainerSaveManager().getSavedContainers()
        
        
        for container in containers{
            self.addSubview(container)
            
            
        }
        

        
        
    }
    

    
    
    public func showButtons(delegate: RoundTextButtonDelegate){

        
        
        
        editButton = RoundTextButton()
        editButton.buttonTitle = "Button"
        editButton.delegate = delegate
        contentView.addSubview(editButton)
        
        addButton = RoundTextButton()
        addButton.buttonTitle = "Add"
        addButton.delegate = delegate
        contentView.addSubview(addButton)
        
        setConstraints()
        

    }
    
    public func setConstraints(){
        
        addButton.translatesAutoresizingMaskIntoConstraints = false
        editButton.translatesAutoresizingMaskIntoConstraints = false
        
        if let addButton = addButton{
            let center = NSLayoutConstraint(item: addButton,
                                          attribute: .centerX,
                                          relatedBy: .equal,
                                          toItem: contentView,
                                          attribute: .centerX,
                                          multiplier: 1,
                                          constant: 0)
            
            let top = NSLayoutConstraint(item: addButton,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: tbb,
                                         attribute: .bottom,
                                         multiplier: 1,
                                         constant: 20)
            
            
            contentView.addConstraints([center, top])
        }
        
        if let editButton = editButton{
            let center = NSLayoutConstraint(item: editButton,
                                          attribute: .centerX,
                                          relatedBy: .equal,
                                          toItem: contentView,
                                          attribute: .centerX,
                                          multiplier: 1,
                                          constant: 0)
        
            
            let underAddButton = NSLayoutConstraint(item: editButton,
                                                    attribute: .top,
                                                    relatedBy: .equal,
                                                    toItem: addButton,
                                                    attribute: .bottom,
                                                    multiplier: 1,
                                                    constant: 20)
            
            contentView.addConstraints([center, underAddButton])

            
        }
        

        
        

    }
    

    
    
    
    
    
    
    
    
}
