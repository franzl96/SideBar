//
//  ViewController.swift
//  SideBar
//
//  Created by Franz Murner on 11.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

class MainViewController: NSViewController {
    
    var effectView: NSVisualEffectView!
    var effectViewFlag: NSVisualEffectView!
    var controllFlagButton: NSButton!
    let sideKickButtonHight: CGFloat = 200
    var timer: Timer?
    var containerController: ContainerController!
    
    
    var urls: [URL] = [URL]()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear() {
        setWindowSize()
        setViewTransparent()
        setEffectView()
        setEffectViewFlag()
        addSidekickButton()
//        testButton()
        

        
        containerController = ContainerController(frame: NSRect(x: 20, y: 0, width: 300, height: view.frame.size.height))
        
        view.addSubview(containerController)
        
        containerController.showButtons(delegate: self)
        

        
//        containerController.showContainers()
        

        



        
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    //MARK:- Methodes
    private func setWindowSize(){

        
        if let screenSize = view.window?.screen?.visibleFrame{
            var presetFrame = screenSize
            presetFrame.size.width = 400

            presetFrame.origin.x = -300
            view.window?.setFrame(presetFrame, display: true)
            view.window?.level = .popUpMenu
        }
    }
    
    private func setViewTransparent(){
        view.window?.backgroundColor = .clear
        view.window?.isOpaque = false
    }
    
    
    
    
    
    
    private func setEffectView(){
        var frame = view.window?.frame ?? NSRect.zero
        frame.size.width = 300
        frame.origin.x = 0
        effectView = NSVisualEffectView(frame: frame)
        effectView.material = .hudWindow
        effectView.state = .active
//        view.addSubview(effectView)
    }
    
    private func setEffectViewFlag(){
        let frame = NSRect(x: 350, y: effectView.frame.size.height / 2 - sideKickButtonHight / 2, width: 20, height: sideKickButtonHight)
        
        effectViewFlag = NSVisualEffectView(frame: frame)
        effectViewFlag.material = .hudWindow
        effectViewFlag.state = .active
        
        view.addSubview(effectViewFlag)
    }
    
    private func addSidekickButton(){
        controllFlagButton = NSButton(frame: CGRect(x: 0, y: 0, width: 20, height: sideKickButtonHight))
        controllFlagButton.title = "Einblenden"
        controllFlagButton.rotate(byDegrees: 270)
        controllFlagButton.setButtonType(.toggle)
        controllFlagButton.isBordered = false
        controllFlagButton.target = self
        controllFlagButton.action = #selector(sideKickButtonPressed)
        
        effectViewFlag.addSubview(controllFlagButton)
    }
    
    @objc func sideKickButtonPressed(){
        if controllFlagButton.state == .off{
            controllFlagButton.title = "Einblenden"
            showView()
        }else{
            controllFlagButton.title = "Ausblenden"
            hideView()
        }
    }
    
    
    func showView(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.001, repeats: true, block: { (timer) in
            let currentOrgin = self.view.window?.frame.origin ?? CGPoint.zero
                if currentOrgin.x <= -350{
                timer.invalidate()
                self.view.window?.setFrameOrigin(NSPoint(x: -350, y: currentOrgin.y))
            }else{
                self.view.window?.setFrameOrigin(NSPoint(x: currentOrgin.x - 20, y: currentOrgin.y))
            }
        })
    }
    
    func hideView(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.001, repeats: true, block: { (timer) in
            let currentOrgin = self.view.window?.frame.origin ?? CGPoint.zero
            print(currentOrgin)
            if currentOrgin.x >= 0{
                timer.invalidate()
                self.view.window?.setFrameOrigin(NSPoint(x: 0, y: currentOrgin.y))
            }else{
                self.view.window?.setFrameOrigin(NSPoint(x: currentOrgin.x + 20, y: currentOrgin.y))
            }
        })
    }
    
    
    func testButton(){
        var button = NSButton(frame: NSRect(origin: CGPoint(x: 30, y: 30), size: CGSize(width: 100, height: 20)))
        button.title = "Ort auswählen"
        button.target = self
        button.action = #selector(testButtonAction)
        
        view.addSubview(button)
        
        var buttonII = NSButton(frame: NSRect(origin: CGPoint(x: 100, y: 100), size: CGSize(width: 100, height: 20)))
        buttonII.title = "Open Again"
        buttonII.target = self
        buttonII.action = #selector(testButtonActionII)
        
        view.addSubview(buttonII)
        
        var buttonIII = NSButton(frame: NSRect(origin: CGPoint(x: 200, y: 500), size: CGSize(width: 100, height: 100)))
        buttonIII.title = "Show Files"
        buttonIII.target = self
        buttonIII.action = #selector(showContents)
        
        view.addSubview(buttonIII)
    }
    
    
    @objc func testButtonAction(){
        //FileManagementHelper.userDefaultDeleteAll()
        
        let url = FileManagementHelper.promptForWorkingDirectoryPermission()
        
        guard let saveURL = url else {
            print("Auswahlvorgang abgebrochen!")
            return
        }
        
        FileManagementHelper.saveBookmarkData(for: saveURL)
        
        print("Data Bookmark saved")
    }
    
    @objc func testButtonActionII(){
        let bookmarkURLs = FileManagementHelper.loadBookmarkedURLs()
        
        guard !bookmarkURLs.isEmpty else {
            print("No saved bookmarks!")
            return
        }
        
        for bookmarkURL in bookmarkURLs{
            print(bookmarkURL.absoluteString)
        }
        
        let urlString = "file:///Users/franzmurner/Documents/"
        
        guard let foundURL = FileManagementHelper.searchURL(searchedURL: urlString, urls: bookmarkURLs) else {
            print("No bookmark found please choose your filesystem")
            return
        }
                
        
        NSWorkspace.shared.open(foundURL)
        
        
    }
    
    @objc func showContents(){
        let bookmarkURLs = FileManagementHelper.loadBookmarkedURLs()
        
        do{
            let directionaryContents = try FileManager.default.contentsOfDirectory(at: bookmarkURLs.first!, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            
            print(directionaryContents)
        }catch{
            print("No Contents!!")
        }
    }
    
    func addANewDir(){
    
        let url = FileManagementHelper.promptForWorkingDirectoryPermission()
        
        guard let saveURL = url else {
            print("Auswahlvorgang abgebrochen!")
            return
        }
        
        FileManagementHelper.saveBookmarkData(for: saveURL)
        containerController.addURL(url: saveURL)
        containerController.showContainers()
        
        print("Data Bookmark saved")
    }
    
    
    
}

extension MainViewController: RoundTextButtonDelegate{

    func buttonPressed(name: String) {
        print(name)

        
        if name == "Add"{
            addANewDir()
        }
    }
    
    
    
    
}
