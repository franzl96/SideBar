//
//  ToolbarBox.swift
//  SideBar
//
//  Created by Franz Murner on 04.07.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

class ToolbarBox: NSView {
    
    
    private var toolbarBox: TitleBarBox!
    private var gridView: NSGridView!
    var editButton: RoundTextButton!

    
    

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        
        setupView()
        addGridView()

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){

        
        editButton = RoundTextButton(frame: NSRect.init(x: self.frame.width / 2 - 50, y: 800, width: 100, height: 20))

        self.addSubview(editButton)
    }
    
    private func addGridView(){
        
        let button = NSButton(frame: NSRect(x: 0, y: 0, width: 40, height: 20))
        let buttonII = NSButton(frame: NSRect(x: 0, y: 0, width: 40, height: 20))

        let empty = NSGridCell.emptyContentView

        
        gridView = NSGridView(views: [[button, empty, buttonII]])
//        gridView.wantsLayer = true
//        gridView.layer?.backgroundColor = NSColor.blue.cgColor
//        gridView.setContentHuggingPriority(NSLayoutConstraint.Priority(600), for: .horizontal)
//        gridView.setContentHuggingPriority(NSLayoutConstraint.Priority(600), for: .vertical)
        gridView.frame = NSRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)

        toolbarBox.addSubview(gridView)
    }
    
    
    
}
