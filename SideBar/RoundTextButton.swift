//
//  EditButton.swift
//  SideBar
//
//  Created by Franz Murner on 03.07.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

protocol RoundTextButtonDelegate {
    func buttonPressed(name: String)->Void
}


class RoundTextButton: NSButton {
    
    var hilightedColor: NSColor = NSColor.systemBlue
    
    var normalColor: NSColor = NSColor.white.withAlphaComponent(0.8){
        didSet{
            self.layer?.backgroundColor = normalColor.cgColor
        }
    }
    
    var buttonTitle: String = "Button" {
        didSet{
            self.title = self.buttonTitle
        }
    }
    
    override var intrinsicContentSize: NSSize{
        var size = super.intrinsicContentSize
        size.width += 20
        size.height += 5
        
        return size
    }
    
    var mouseInColor: NSColor = NSColor.white
    
    var mouseInAnimations: Bool = true

    public var delegate: RoundTextButtonDelegate?
    
    
    init() {
        super.init(frame: .zero)
        drawButton()

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func drawButton(){
        
        self.title = buttonTitle
        self.state = .off
        self.isBordered = false
        
        self.wantsLayer = true
        self.layer?.backgroundColor = normalColor.cgColor
        self.layer?.cornerRadius = intrinsicContentSize.height / 2
        
        
        self.contentTintColor = .black
        self.setButtonType(.momentaryPushIn)
        
        let area = NSTrackingArea.init(rect: self.bounds,
                                       options: [.mouseEnteredAndExited, .activeAlways],
                                       owner: self,
                                       userInfo: nil)
        self.addTrackingArea(area)
        
    }
    
    override func mouseDown(with event: NSEvent) {
        layer?.backgroundColor = hilightedColor.cgColor
    }
    
    override func mouseUp(with event: NSEvent) {
        if mouseInAnimations{
            layer?.backgroundColor = mouseInColor.cgColor
        }else{
            layer?.backgroundColor = normalColor.cgColor
        }
        
        
        
        delegate?.buttonPressed(name: title)
    }
    
    override func mouseEntered(with event: NSEvent) {
        guard mouseInAnimations == true else {
            return
        }
        
        layer?.backgroundColor = mouseInColor.cgColor
    }
    
    override func mouseExited(with event: NSEvent) {
        guard mouseInAnimations == true else {
            return
        }
        
        layer?.backgroundColor = normalColor.cgColor
    }
    
    
}
