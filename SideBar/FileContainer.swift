//
//  FileContainer.swift
//  SideBar
//
//  Created by Franz Murner on 19.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

class FileContainer: TitleBarBox{
    
    var containerCollectionView: NSCollectionView!
    var containerURLPath: URL?{
        didSet{
            do{
                guard let containerURLPath = containerURLPath else {
                    return
                }
                
                childURLS = try FileManager.default.contentsOfDirectory(at: containerURLPath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                
                containerCollectionView.reloadData()
                
                titleBarText = containerURLPath.lastPathComponent
                
            }catch{
                print("No Contents!!")
            }
        }
    }
    
    
//    init() {
//        addCollectionView()
//    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private var childURLS = [URL]()

    
    
    
    
    
    func addCollectionView(){
        
        
        let scrollView = NSScrollView(frame: NSRect(origin: CGPoint(x: 0, y: 0), size: NSSize(width: self.frame.width, height: self.frame.height - titleBarHeight)))

        let viewLayout = NSCollectionViewFlowLayout()
        viewLayout.minimumLineSpacing = 30
        viewLayout.sectionInset = NSEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        viewLayout.itemSize = CGSize(width: 70, height: 100)
        viewLayout.scrollDirection = .horizontal
        
        
        containerCollectionView = NSCollectionView(frame: .zero)
        
        scrollView.documentView = containerCollectionView

        containerCollectionView.collectionViewLayout = viewLayout
        containerCollectionView.delegate = self
        containerCollectionView.dataSource = self
        containerCollectionView.backgroundColors = [.clear]
        
        self.addSubview(scrollView)
        
        containerCollectionView.register(FileButtonItem.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("FileButtonItem"))
    }
    
}

extension FileContainer: NSCollectionViewDataSource{
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return childURLS.count
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let fileButtonItem = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier("FileButtonItem"), for: indexPath) as! FileButtonItem
        fileButtonItem.specificURL = childURLS[indexPath.item]
        
        return fileButtonItem
    }
    
    
}

extension FileContainer: NSCollectionViewDelegate{
    
}
