//
//  FileButtons.swift
//  SideBar
//
//  Created by Franz Murner on 19.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

class FileButton: NSButton {
    
    var connectedURL: URL?{
        didSet{
            guard let connectedURL = connectedURL else {
                return
            }
            
            self.image = NSWorkspace.shared.icon(forFile: connectedURL.path)
        }
    }

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        
        // Drawing code here.
    }
    

    
    override func mouseDown(with event: NSEvent) {
        
        guard let connectedURL = connectedURL else {
            return
        }
        NSWorkspace.shared.open(connectedURL)
    }
    
    
    
}
