//
//  FileManagementHelper.swift
//  SideBar
//
//  Created by Franz Murner on 11.06.20.
//  Copyright © 2020 Franz Murner. All rights reserved.
//

import Cocoa

class FileManagementHelper {
    
    static func userDefaultDeleteAll(){
        let userDefaults = UserDefaults()
        userDefaults.set([Data](), forKey: "BookmarkData")
    }
    
    static func userDefaultSave(bookmarkdata: Data){
        let userDefaults = UserDefaults()
        
        var dataArray = userDefaults.array(forKey: "BookmarkData")
        
        if dataArray == nil{
            dataArray = [Data]()
        }
        
        dataArray?.append(bookmarkdata)
        
        userDefaults.set(dataArray, forKey: "BookmarkData")
        
    }
    
    static func loadBookmarkedURLs()->[URL]{
        let userDefaults = UserDefaults()
        let bookmarkData = userDefaults.array(forKey: "BookmarkData") as? [Data]
        var urls = [URL]()
        
        guard let saveBookmarkData = bookmarkData else {return urls}
        
        for data in saveBookmarkData{
            let url = restoreFileAccess(with: data)
        
            guard let saveURL = url else {
                continue
            }
            
            urls.append(saveURL)
        }
        
        return urls
    }
    
    static func searchURL(searchedURL: String, urls: [URL])->URL?{

        for url in urls{
            
            guard searchedURL == url.absoluteString else{
                continue
            }
            
            return url
        }
        
        return nil
    }
    
    static func promptForWorkingDirectoryPermission() -> URL? {
        let openPanel = NSOpenPanel()
        openPanel.message = "Choose your directory"
        openPanel.prompt = "Choose"
        openPanel.allowsOtherFileTypes = false
        openPanel.canChooseFiles = true
        openPanel.canChooseDirectories = true
        
        let _ = openPanel.runModal()
        print(openPanel.urls) // this contains the chosen folder
        return openPanel.urls.first
    }
    
    static func saveBookmarkData(for workDir: URL) {
        do {
            let bookmarkData = try workDir.bookmarkData(options: .withSecurityScope, includingResourceValuesForKeys: nil, relativeTo: nil)
            

            userDefaultSave(bookmarkdata: bookmarkData)
            
            
        } catch {
            print("Failed to save bookmark data for \(workDir)", error)
        }
    }
    
    static func restoreFileAccess(with bookmarkData: Data) -> URL? {
        do {
            var isStale = false
            let url = try URL.init(resolvingBookmarkData: bookmarkData, options: .withSecurityScope, relativeTo: nil, bookmarkDataIsStale: &isStale)
            
            if isStale {
                // bookmarks could become stale as the OS changes
                print("Bookmark is stale, need to save a new one... ")
                saveBookmarkData(for: url)
            }
            _ = url.startAccessingSecurityScopedResource()
            return url
        } catch {
            print("Error resolving bookmark:", error)
            return nil
        }
    }
    

}
